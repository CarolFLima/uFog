#include <stdio.h>
#include <stdlib.h>
#define MAX_ITEMS 5

typedef struct _state State;

State * state_new(void);

void setItemInState(State * state, char * item, int position);

char * getItemFromState(State * state, int position);

void setState(State * state, char * items);


  #include <espressif/esp_common.h>
  #include <esp/uart.h>
  #include <FreeRTOS.h>
  #include <task.h>
  //#include <queue.h>
  #include <timers.h>
  #include <string.h>
  #include <semphr.h>
  #include <task.h>
  #include <ssd1306/ssd1306.h>

  #include <ssid_config.h>

  #include <espressif/esp_sta.h>
  #include <espressif/esp_wifi.h>

  #include <paho_mqtt_c/MQTTESP8266.h>
  #include <paho_mqtt_c/MQTTClient.h>

  #include "ssl_connection.h"

  //#define MQTT_PUB_TOPIC "esp8266/status"
  #define MQTT_PUB_TOPIC_BOMBA "$aws/things/iBomba/shadow/update"
  #define MQTT_PUB_TOPIC_NEMO "$aws/things/iNemo/shadow/update"
  #define MQTT_SUB_TOPIC "uFog/control"
  #define GPIO_LED 2
  #define MQTT_PORT 8883

  //------MQTT_START--------

  #define MQTT_HOST ("iot.eclipse.org")
  #define MQTT_PORT2 1883

  #define MQTT_USER NULL
  #define MQTT_PASS NULL

  // SemaphoreHandle_t wifi_alive2;
  QueueHandle_t publish_queue_things;
  #define PUB_MSG_LEN 16

  //------MQTT_END----------

  #define GET_STATUS "E5"
  #define GET_LEVEL "E6"

  char merge[2] = {'79', '\0'};

  /* certs, key, and endpoint */
  extern char *ca_cert, *client_endpoint, *client_cert, *client_key;

  SemaphoreHandle_t wifi_alive;
  static int ssl_reset;
  static SSLConnection *ssl_conn;
  static QueueHandle_t publish_queue;


  #include "stateScreen.h"

  #define LOAD_ICON_X 54
  #define LOAD_ICON_Y 42
  #define LOAD_ICON_SIZE 20

  #define CIRCLE_COUNT_ICON_X 100
  #define CIRCLE_COUNT_ICON_Y 52

  /* Remove this line if your display connected by SPI */
  #define I2C_CONNECTION

  #ifdef I2C_CONNECTION
      #include <i2c/i2c.h>
  #endif
  #include "fonts/fonts.h"

  /* Change this according to you schematics and display size */
  #define DISPLAY_WIDTH  128
  #define DISPLAY_HEIGHT 64

  #ifdef I2C_CONNECTION
      #define PROTOCOL SSD1306_PROTO_I2C
      #define ADDR     SSD1306_I2C_ADDR_0
      #define SCL_PIN  5
      #define SDA_PIN  4
  #else
      #define PROTOCOL SSD1306_PROTO_SPI4
      #define CS_PIN   5
      #define DC_PIN   4
  #endif

  //#define DEFAULT_FONT FONT_FACE_TERMINUS_6X12_ISO8859_1

  #define PIN_CLK  14
  #define PIN_DT   12
  #define PIN_SW   13

  #define LATCHSTATE 3

  uint8_t oldState = 3;
  uint8_t position = 0;
  uint8_t positionExt = 0;

  uint8_t newPosition;
  uint8_t currPosition = 0;

  uint8_t initialSelected = 0;

  State * s1;
  State * s2;
  State * s3;
  State * s4;
  State * current_state;

  SemaphoreHandle_t semap_handle;

  uint16_t count = 0;
  char * msgReceived[10];
  char * msgStd = "AA";

  const int8_t KNOBDIR[] = {
    0, -1,  1,  0,
    1,  0,  0, -1,
    -1,  0,  0,  1,
  0,  1, -1,  0  };

  /* Declare device descriptor */
  static const ssd1306_t dev = {
      .protocol = PROTOCOL,
  #ifdef I2C_CONNECTION
      .addr     = ADDR,
  #else
      .cs_pin   = CS_PIN,
      .dc_pin   = DC_PIN,
  #endif
      .width    = DISPLAY_WIDTH,
      .height   = DISPLAY_HEIGHT
  };

  void tick()
  {
    uint8_t sig1 = gpio_read(PIN_CLK);
    uint8_t sig2 = gpio_read(PIN_DT);

    uint8_t thisState = sig1 | (sig2 << 1);

    if(oldState != thisState){
      position += KNOBDIR[thisState | (oldState<<2)];

      if(thisState == LATCHSTATE)
        positionExt = position >> 2;

      oldState = thisState;
    }
  }

  /* Local frame buffer */
  static uint8_t buffer[DISPLAY_WIDTH * DISPLAY_HEIGHT / 8];

  TimerHandle_t fps_timer_handle = NULL; // Timer handler
  TimerHandle_t font_timer_handle = NULL;

  uint8_t frame_done = 0; // number of frame send.
  uint8_t fps = 0; // image per second.

  const font_info_t *font = NULL; // current font
  const font_info_t *font_large = NULL; // current font
  font_face_t font_face = 0;

  #define SECOND (1000 / portTICK_PERIOD_MS)

  void init_display()
  {
    printf("Display init\n");

    ssd1306_set_whole_display_lighting(&dev, false);

    font = font_builtin_fonts[1];
    font_large = font_builtin_fonts[2];

  }

  void update_display()
  {
    char * itemFromState = getItemFromState(current_state, 0);
    int i = 1;

    ssd1306_fill_rectangle(&dev, buffer, 0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT, OLED_COLOR_BLACK);

    if(itemFromState == "1"){
      // Desenha uma lista
      while(getItemFromState(current_state, i) != ""){
        if((currPosition%2) == (i-1))
          ssd1306_draw_string(&dev, buffer, font, 15, (i-1) * 25,  getItemFromState(current_state, i), OLED_COLOR_BLACK, OLED_COLOR_WHITE);
        else
          ssd1306_draw_string(&dev, buffer, font, 15, (i-1) * 25,  getItemFromState(current_state, i), OLED_COLOR_WHITE, OLED_COLOR_BLACK);
        i++;
      }
    } else if(itemFromState == "2"){
      // Desenha a tela de Loading ou de Error
      ssd1306_draw_string(&dev, buffer, font, 32, 0,  getItemFromState(current_state, 1), OLED_COLOR_WHITE, OLED_COLOR_BLACK);

      for (uint8_t i = 0; i < 10; i++) {
        if ((count >> i) & 0x01){
          ssd1306_draw_circle(&dev, buffer, 64, 40, i, OLED_COLOR_BLACK);
        }
      }

      count = count == 0x03FF ? 0 : count + 1;

      for (uint8_t i = 0; i < 10; i++) {
        if ((count >> i) & 0x01){
          ssd1306_draw_circle(&dev,buffer, 64, 40, i, OLED_COLOR_WHITE);
        }
      }
    } else if(itemFromState == "3"){
      // Desenha o Dialog
      if(currPosition%2 == 0){
        ssd1306_draw_string(&dev, buffer, font, 15, 0,  "<--", OLED_COLOR_BLACK, OLED_COLOR_WHITE);
        ssd1306_draw_string(&dev, buffer, font, 90, 0,  getItemFromState(current_state, 1), OLED_COLOR_WHITE, OLED_COLOR_BLACK);
      } else if(currPosition%2 == 1){
        ssd1306_draw_string(&dev, buffer, font, 15, 0,  "<--", OLED_COLOR_WHITE, OLED_COLOR_BLACK);
        ssd1306_draw_string(&dev, buffer, font, 90, 0,  getItemFromState(current_state, 1), OLED_COLOR_BLACK, OLED_COLOR_WHITE);
      }
      ssd1306_draw_string(&dev, buffer, font_large, 55, 26,  getItemFromState(current_state, 2), OLED_COLOR_WHITE, OLED_COLOR_BLACK);
    //  printf("Initial selected eh %s\n", getItemFromState(current_state, 2));
      if(initialSelected == 0)
        ssd1306_draw_string(&dev, buffer, font, 90, 40,  "%", OLED_COLOR_WHITE, OLED_COLOR_BLACK);
      else
        ssd1306_draw_string(&dev, buffer, font, 90, 40,  "km", OLED_COLOR_WHITE, OLED_COLOR_BLACK);
    }
    if (ssd1306_load_frame_buffer(&dev, buffer)){
      //add tratamento de erro
    }

    vTaskDelay(10);
  }

  void encoder_task(void *pvParameters)
  {
      uint8_t tempPosition;
      uint8_t counter = 0;

      init_display();
      update_display();

      char msg[10]; //mudar pra um define
      memset(msg, 0, 10);

      while(1) {
        tick();
        newPosition = positionExt;

        // Na tela de loading
        if(current_state == s2){
          printf("Ta dentro do loading %c\n", msgReceived[0]);
          if(strstr(msgReceived, "A") != NULL || strstr(msgReceived, "B") != NULL) {
            printf("--------------- Recebeu um A ou B\n");
            msgReceived[0] = "Z";
            snprintf(msg, sizeof(msg), "%c", msgReceived[1]);
            setItemInState(s4, msg, 2);
            current_state = s4;
          }
          if(counter == 9){
            setItemInState(s2, "ERROR", 1);
            current_state = s3;
            currPosition = 0;
            counter = 0;
          }
          counter++;
          update_display();
          vTaskDelay( 500 / portTICK_PERIOD_MS );
        }
        // Rotacao do encoder
        if(currPosition != newPosition){
          currPosition = newPosition;
          update_display();
        }
        // Selecionou opcao do encoder
        if(gpio_read(PIN_SW) == 0){
          printf("Selected item\n");
          tempPosition = currPosition % 2;
          if(current_state == s1){
            setItemInState(s2, "LOADING", 1);
            initialSelected = tempPosition;
            currPosition = 0; // zerando o encoder

            //MANDANDO PRA S4 - TIRAR ISSO DEPS

            if(initialSelected == 1){
              setItemInState(s4, "WIN", 1);
            } else {
              setItemInState(s4, "OFF", 1);
            }
            current_state = s2;

            snprintf(msg, sizeof(msg), "%c", 0xE5); //GET_STATUS
            xQueueSend(publish_queue_things, (void *) msg, 0);
            //Trocar por um timer handle
            update_display();
            //vTaskDelay(5000 / portTICK_PERIOD_MS);

            // if(strstr(msgReceived, '79') != NULL) {
            //     currPosition = 0;
            //     setItemInState(s4, "ON", 1);
            //     current_state = s4;
            // } else if (strstr(msgReceived, '1') != NULL) {
            //     currPosition = 0;
            //     setItemInState(s4, "OFF", 1);
            //     current_state = s4;
            // } else {
            //     currPosition = initialSelected;
            //     current_state = s3;
            // }
            // printf(" ======= MSG RECEIVED: %s\n", msgReceived);
            // strcpy(msgReceived, msgStd);

          } else if(current_state == s3){
            currPosition = initialSelected;
            current_state = s1;

          } else if(current_state == s4){
            if(tempPosition == 0){ //back to 1
              currPosition = initialSelected;
              current_state = s1;
            } else if(tempPosition == 1){
              //Aqui tratamos diferente para bomba ou para o anemo
              if(initialSelected == 0){
                if(getItemFromState(current_state, 1) == "ON"){
                  printf("Entrou no on\n");
                  merge[0] = '1';
                  snprintf(msg, sizeof(msg), merge);
                  setItemInState(s4, "OFF", 1);
                } else if(getItemFromState(current_state, 1) == "OFF"){
                  printf("Entrou no off\n");
                  snprintf(msg, sizeof(msg), merge);
                  printf("passou daqui 1\n");
                  setItemInState(s4, "ON", 1);
                  printf("passou daqui 2\n");
                }
                xQueueSend(publish_queue_things, (void *) msg, 0);
              }
            }
          }
          update_display();
          vTaskDelay(200);
        }
      }
  }

  void state_init()
  {
      s1 = state_new();
      s2 = state_new();
      s3 = state_new();
      s4 = state_new();
      char * tempItems1[] = {"1", "iBomba", "iNemo", "", ""};
      setState(s1, tempItems1);
      char * tempItems2[] = {"2", "LOADING", "", "", ""};
      setState(s2, tempItems2);
      char * tempItems3[] = {"2", "ERROR", "", "", ""};
      setState(s3, tempItems3);
      char * tempItems4[] = {"3", "OFF", "73", "", ""};
      setState(s4, tempItems4);

      current_state = s1;
  }

  static void topic_received(mqtt_message_data_t *md) {
      mqtt_message_t *message = md->message;
      int i;
      char msg[64];

      printf("Received: ");
      for (i = 0; i < md->topic->lenstring.len; ++i)
          printf("%c", md->topic->lenstring.data[i]);

      printf(" = ");
      for (i = 0; i < (int) message->payloadlen; ++i)
          printf("%c", ((char *) (message->payload))[i]);
      printf("\r\n");

      // printf("Passou pelo topic received\n");
      // if(memcmp(message->payload, '10', 1)){
      //   memcpy(msgReceived, message->payload+1, 1);
      // } else if(memcmp(message->payload, '15', 1)){
      //   memcpy(msgReceived, message->payload+2, 1);
      // }
      // printf("Passou pelo topic received 2\n");

      // if(((char *) (message->payload))[0] == 0x15){
      //   printf("Recebeu da bomba\n");
      // } else if(((char *) (message->payload))[0] == 0x10){
      //   printf("Recebeu do anemo\n");
      // }
      char * merge[2];
      memcpy(msgReceived, ((char *) (message->payload)), 2);
      snprintf(merge, sizeof(merge), "%c", ((char *) (message->payload))[1]);

      if(((char *) (message->payload))[0] == 'A'){
        //como fazer pra ele retornar o get status separado
        //setItemInState(s4, merge, 2);
        printf("Recebeu da bomba %s\n", merge);
      } else if(((char *) (message->payload))[0] == 'B'){
        //setItemInState(s4, merge, 2);
        printf("Recebeu do anemo\n");
      }

      printf("Recebeu mensagem! %s\n", ((char *) (message->payload)));
      for( i = 0; i < (int)message->payloadlen; ++i){
        printf(" %d, %c ", i, ((char *)(message->payload))[i]);
      }

      xQueueSend(publish_queue, (void *) msg, 0);
  }

  static const char *get_my_id(void) {
      // Use MAC address for Station as unique ID
      static char my_id[13];
      static bool my_id_done = false;
      int8_t i;
      uint8_t x;
      if (my_id_done)
          return my_id;
      if (!sdk_wifi_get_macaddr(STATION_IF, (uint8_t *) my_id))
          return NULL;
      for (i = 5; i >= 0; --i) {
          x = my_id[i] & 0x0F;
          if (x > 9)
              x += 7;
          my_id[i * 2 + 1] = x + '0';
          x = my_id[i] >> 4;
          if (x > 9)
              x += 7;
          my_id[i * 2] = x + '0';
      }
      my_id[12] = '\0';
      my_id_done = true;
      return my_id;
  }

  static int mqtt_ssl_read(mqtt_network_t * n, unsigned char* buffer, int len,
          int timeout_ms) {
      int r = ssl_read(ssl_conn, buffer, len, timeout_ms);
      if (r <= 0
              && (r != MBEDTLS_ERR_SSL_WANT_READ
                      && r != MBEDTLS_ERR_SSL_WANT_WRITE
                      && r != MBEDTLS_ERR_SSL_TIMEOUT)) {
          // printf("%s: TLS read error (%d), resetting\n\r", __func__, r);
          ssl_reset = 1;
      };
      return r;
  }

  static int mqtt_ssl_write(mqtt_network_t* n, unsigned char* buffer, int len,
          int timeout_ms) {
      int r = ssl_write(ssl_conn, buffer, len, timeout_ms);
      if (r <= 0
              && (r != MBEDTLS_ERR_SSL_WANT_READ
                      && r != MBEDTLS_ERR_SSL_WANT_WRITE)) {
          // printf("%s: TLS write error (%d), resetting\n\r", __func__, r);
          ssl_reset = 1;
      }
      return r;
  }

  static void mqtt_task(void *pvParameters) {
      int ret = 0;
      struct mqtt_network network;
      mqtt_client_t client = mqtt_client_default;
      char mqtt_client_id[20];
      uint8_t mqtt_buf[100];
      uint8_t mqtt_readbuf[100];
      mqtt_packet_connect_data_t data = mqtt_packet_connect_data_initializer;

      memset(mqtt_client_id, 0, sizeof(mqtt_client_id));
      strcpy(mqtt_client_id, "ESP-");
      strcat(mqtt_client_id, get_my_id());

      ssl_conn = (SSLConnection *) malloc(sizeof(SSLConnection));
      while (1) {
          xSemaphoreTake(wifi_alive, portMAX_DELAY);

          printf("%s: started\n\r", __func__);
          ssl_reset = 0;
          ssl_init(ssl_conn);
          ssl_conn->ca_cert_str = ca_cert;
          ssl_conn->client_cert_str = client_cert;
          ssl_conn->client_key_str = client_key;

          mqtt_network_new(&network);
          network.mqttread = mqtt_ssl_read;
          network.mqttwrite = mqtt_ssl_write;

          printf("%s: connecting to MQTT server %s ... ", __func__, client_endpoint);
          ret = ssl_connect(ssl_conn, client_endpoint, MQTT_PORT);

          if (ret) {
              printf("error: %d\n\r", ret);
              ssl_destroy(ssl_conn);
              taskYIELD();
              continue;
          }
          printf("%s: done\n\r", __func__);
          mqtt_client_new(&client, &network, 5000, mqtt_buf, 100, mqtt_readbuf,
                  100);

          data.willFlag = 0;
          data.MQTTVersion = 4;
          data.cleansession = 1;
          data.clientID.cstring = mqtt_client_id;
          data.username.cstring = NULL;
          data.password.cstring = NULL;
          data.keepAliveInterval = 1000;
          printf("%s: Send MQTT connect ... ", __func__);
          ret = mqtt_connect(&client, &data);
          if (ret) {
              printf("%s: error: %d\n\r", __func__, ret);
              ssl_destroy(ssl_conn);
              taskYIELD();
              continue;
          }
          printf("%s: done\r\n", __func__);
          mqtt_subscribe(&client, MQTT_SUB_TOPIC, MQTT_QOS1, topic_received);
          xQueueReset(publish_queue);

          while (wifi_alive && !ssl_reset) {
              char msg[64];
              while (xQueueReceive(publish_queue, (void *) msg, 0) == pdTRUE) {

                  snprintf(msg, sizeof(msg), "{\"state\":{\"reported\":{\"value\":\"%d\"}}}", getItemFromState(current_state, 2));

                  printf("%s: Publishing: %s\r\n", __func__, msg);

                  mqtt_message_t message;
                  message.payload = msg;
                  message.payloadlen = strlen(msg);
                  message.dup = 0;
                  message.qos = MQTT_QOS1;
                  message.retained = 0;
                  if(initialSelected == 0)
                    ret = mqtt_publish(&client, MQTT_PUB_TOPIC_BOMBA, &message);
                  else
                    ret = mqtt_publish(&client, MQTT_PUB_TOPIC_NEMO, &message);
                  if (ret != MQTT_SUCCESS) {
                      printf("%s: error while publishing message: %d\n", __func__, ret);
                      break;
                  }
              }

              ret = mqtt_yield(&client, 1000);
              if (ret == MQTT_DISCONNECTED)
                  break;
          }
          printf("%s: Connection dropped, request restart\n\r", __func__);
          ssl_destroy(ssl_conn);
          taskYIELD();
      }
  }

  static void  mqtt_task_things(void *pvParameters)
  {
      int ret         = 0;
      struct mqtt_network network;
      mqtt_client_t client   = mqtt_client_default;
      char mqtt_client_id[20];
      uint8_t mqtt_buf[100];
      uint8_t mqtt_readbuf[100];
      mqtt_packet_connect_data_t data = mqtt_packet_connect_data_initializer;

      mqtt_network_new( &network );
      memset(mqtt_client_id, 0, sizeof(mqtt_client_id));
      strcpy(mqtt_client_id, "ESP-");
      strcat(mqtt_client_id, get_my_id());

      while(1) {
          xSemaphoreTake(wifi_alive, portMAX_DELAY);
          printf("%s: started\n\r", __func__);
          // printf("%s: (Re)connecting to MQTT server %s ... ",__func__, MQTT_HOST);
          ret = mqtt_network_connect(&network, MQTT_HOST, MQTT_PORT2);
          if( ret ){
              // printf("error: %d\n\r", ret);
              taskYIELD();
              continue;
          }
          // printf("done\n\r");
          mqtt_client_new(&client, &network, 5000, mqtt_buf, 100,
                        mqtt_readbuf, 100);

          data.willFlag       = 0;
          data.MQTTVersion    = 3;
          data.clientID.cstring   = mqtt_client_id;
          data.username.cstring   = MQTT_USER;
          data.password.cstring   = MQTT_PASS;
          data.keepAliveInterval  = 10;
          data.cleansession   = 0;
          // printf("Send MQTT connect ... ");
          ret = mqtt_connect(&client, &data);
          if(ret){
              // printf("error: %d\n\r", ret);
              mqtt_network_disconnect(&network);
              taskYIELD();
              continue;
          }
          // printf("done\r\n");
          mqtt_subscribe(&client, "/esptopic", MQTT_QOS1, topic_received);
          xQueueReset(publish_queue_things);

          while(1){

              char msg[PUB_MSG_LEN - 1] = "\0";
              while(xQueueReceive(publish_queue_things, (void *)msg, 0) ==
                    pdTRUE){
                  printf("got message to publish\r\n");
                  mqtt_message_t message;
                  message.payload = msg;
                  message.payloadlen = PUB_MSG_LEN;
                  message.dup = 0;
                  message.qos = MQTT_QOS1;
                  message.retained = 0;

                  ret = mqtt_publish(&client, "/beat1", &message);
                  printf("Passou do publish\n");
                  if (ret != MQTT_SUCCESS ){
                      // printf("error while publishing message: %d\n", ret );
                      break;
                  }
              }

              ret = mqtt_yield(&client, 1000);
              if (ret == MQTT_DISCONNECTED)
                  break;
          }
          // printf("Connection dropped, request restart\n\r");
          mqtt_network_disconnect(&network);
          taskYIELD();
      }
  }

  static void  wifi_task(void *pvParameters)
  {
      uint8_t status  = 0;
      uint8_t retries = 30;
      struct sdk_station_config config = {
          .ssid = WIFI_SSID,
          .password = WIFI_PASS,
      };

      printf("WiFi: connecting to WiFi\n\r");
      sdk_wifi_set_opmode(STATION_MODE);
      sdk_wifi_station_set_config(&config);

      while(1)
      {
          while ((status != STATION_GOT_IP) && (retries)){
              status = sdk_wifi_station_get_connect_status();
              printf("%s: status = %d\n\r", __func__, status );
              if( status == STATION_WRONG_PASSWORD ){
                  printf("WiFi: wrong password\n\r");
                  break;
              } else if( status == STATION_NO_AP_FOUND ) {
                  printf("WiFi: AP not found\n\r");
                  break;
              } else if( status == STATION_CONNECT_FAIL ) {
                  printf("WiFi: connection failed\r\n");
                  break;
              }
              vTaskDelay( 1000 / portTICK_PERIOD_MS );
              --retries;
          }

          while ((status = sdk_wifi_station_get_connect_status()) == STATION_GOT_IP) {
              xSemaphoreGive( wifi_alive );
              taskYIELD();
          }
          printf("WiFi: disconnected\n\r");
          // sdk_wifi_station_disconnect();
          vTaskDelay( 1000 / portTICK_PERIOD_MS );
      }
  }

  void user_init(void)
  {
      state_init();

      uart_set_baud(0, 115200);

      gpio_enable(PIN_SW, GPIO_INPUT);
      gpio_enable(PIN_CLK, GPIO_INPUT);
      gpio_enable(PIN_DT, GPIO_INPUT);

      //semap_handle = xSemaphoreCreateMutex();

      vSemaphoreCreateBinary(wifi_alive);

      printf("SDK version:%s\n", sdk_system_get_sdk_version());

  #ifdef I2C_CONNECTION
      i2c_init(SCL_PIN, SDA_PIN);
  #endif

      while (ssd1306_init(&dev) != 0) {
          printf("%s: failed to init SSD1306 lcd\n", __func__);
          vTaskDelay(SECOND);
      }

      ssd1306_set_whole_display_lighting(&dev, true);
      vTaskDelay(SECOND);

      publish_queue = xQueueCreate(3, 16);
      publish_queue_things = xQueueCreate(3, PUB_MSG_LEN);

      // fps_timer_handle = xTimerCreate("fps_timer", SECOND/2, pdTRUE, NULL, fps_timer);
      // xTimerStart(fps_timer_handle, 0);

      xTaskCreate(&wifi_task, "wifi_task", 256, NULL, 2, NULL);
      xTaskCreate(&mqtt_task, "mqtt_task", 2048, NULL, 2, NULL);
      xTaskCreate(&mqtt_task_things, "mqtt_task2", 1024, NULL, 2, NULL);
      xTaskCreate(&encoder_task, "encoder_task", 256, NULL, 2, NULL);

  }
// AWS IoT client endpoint
const char *client_endpoint = "a1uugir7wa1oeu.iot.us-west-2.amazonaws.com";

// AWS IoT device certificate (ECC)
const char *client_cert =
"-----BEGIN CERTIFICATE-----\r\n"
"MIIC7TCCAdWgAwIBAgIVAOt3IZ8YLq8zA9BZEKmgitUC6mHqMA0GCSqGSIb3DQEB\r\n"
"CwUAME0xSzBJBgNVBAsMQkFtYXpvbiBXZWIgU2VydmljZXMgTz1BbWF6b24uY29t\r\n"
"IEluYy4gTD1TZWF0dGxlIFNUPVdhc2hpbmd0b24gQz1VUzAeFw0xNzA1MTkxNzEw\r\n"
"NDBaFw00OTEyMzEyMzU5NTlaMHwxCzAJBgNVBAYTAkJSMRAwDgYDVQQIEwdBbGFn\r\n"
"b2FzMQ8wDQYDVQQHEwZNYWNlaW8xDTALBgNVBAoTBFVGQUwxCzAJBgNVBAsTAklD\r\n"
"MQ8wDQYDVQQDEwZGZWxpcGUxHTAbBgkqhkiG9w0BCQEWDmZjY0BpYy51ZmFsLmJy\r\n"
"MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEm/X2Y265N2QDC8YYJdomDJmybrqD\r\n"
"/1BAMtJiZFMsubTCWV8fIglOYzyFzSjLBnSZwGtpxLgmAYpXfBkOYelFU6NgMF4w\r\n"
"HwYDVR0jBBgwFoAUNV/9pNP1FnsuLS9nd82iCjr+UvEwHQYDVR0OBBYEFO+8LIVZ\r\n"
"UvpOzDAOTFZQWu/NNQjrMAwGA1UdEwEB/wQCMAAwDgYDVR0PAQH/BAQDAgeAMA0G\r\n"
"CSqGSIb3DQEBCwUAA4IBAQALVD1V/+U1fBAukjaZX7tZPfgSX4v6QR+TSScHxmy/\r\n"
"xstl7DdW6sw+R4MbtXoxIdVqIY2FlwbFzcfYrpffTLK7sRTsD+nXxupWe4wU6B1x\r\n"
"8KR50WS5NTyWVp63X+FScQjbkF7TOv0GluDXvvOlb93ywH4I8EnVhdQR4piR1zVH\r\n"
"03cUxjDLp7XYA9MH9SO+s0NovExKKUiyLLyGMlDkyHmbE6GrotRAFdszmj9nlwUW\r\n"
"95Co6b7kshHtMtCownW4aYucsxTcvYFXgcUqfekn2lD9DPf9tUcCaqQhyRSM2N74\r\n"
"cLrP8Hx8g9Jh5jNKTZ+mWkVEorVz5YLSnG3PCLrQeJKB\r\n"
"-----END CERTIFICATE-----\r\n";

// AWS IoT device private key (ECC)
const char *client_key = 
"-----BEGIN EC PARAMETERS-----\r\n"
"BggqhkjOPQMBBw==\r\n"
"-----END EC PARAMETERS-----\r\n"
"-----BEGIN EC PRIVATE KEY-----\r\n"
"MHcCAQEEIK/cCBaSrlILDn2MbSP24fD/EsIy4x7Q2mlmh4GXDg9aoAoGCCqGSM49\r\n"
"AwEHoUQDQgAEm/X2Y265N2QDC8YYJdomDJmybrqD/1BAMtJiZFMsubTCWV8fIglO\r\n"
"YzyFzSjLBnSZwGtpxLgmAYpXfBkOYelFUw==\r\n"
"-----END EC PRIVATE KEY-----\r\n";

#include "stateScreen.h"

struct _state {
	char * screenItems[MAX_ITEMS];
//array [tipo, n elementos]
//["1", "iBomba", "iNemo", "FOrno"]
// ["2", "OFF", "54"];

};

State * state_new(void){
   State * state;
   state = malloc(sizeof(State));
   return state;
}

void setItemInState(State * state, char * item, int position){
    if(position < MAX_ITEMS)
      state->screenItems[position] = item;
}

char * getItemFromState(State * state, int position){
    if(position < MAX_ITEMS)
      return state->screenItems[position];
    else
      return "";
}

void setState(State * state, char * items){
    strcpy(state->screenItems, items);
}

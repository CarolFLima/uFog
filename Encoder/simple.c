#include "espressif/esp_common.h"
#include "esp/uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#define PIN_CLK  14
#define PIN_DT   12
#define PIN_SW   13

#define LATCHSTATE 3

uint8_t oldState = 3;
uint8_t position = 0;
uint8_t positionExt = 0;

const int8_t KNOBDIR[] = {
  0, -1,  1,  0,
  1,  0,  0, -1,
  -1,  0,  0,  1,
0,  1, -1,  0  };

void tick(){
  uint8_t sig1 = gpio_read(PIN_CLK);
  uint8_t sig2 = gpio_read(PIN_DT);

  uint8_t thisState = sig1 | (sig2 << 1);

  if(oldState != thisState){
    position += KNOBDIR[thisState | (oldState<<2)];

    if(thisState == LATCHSTATE)
      positionExt = position >> 2;

    oldState = thisState;
  }
}

void encoder_task(void *pvParameters)
{

    uint8_t newPosition;
    uint8_t currPosition = 0;
    while(1) {
      //tratar aqui o botao pressionado
      tick();

      newPosition = positionExt;
      if(currPosition != newPosition){
        printf("Mudou a posição %d\n", newPosition);
        currPosition = newPosition;
      }

    }
}

static QueueHandle_t mainqueue;

void user_init(void)
{
    uart_set_baud(0, 115200);
    printf("SDK version:%s\n", sdk_system_get_sdk_version());

    gpio_enable(PIN_SW, GPIO_INPUT);
    gpio_enable(PIN_CLK, GPIO_INPUT);
    gpio_enable(PIN_DT, GPIO_INPUT);

    xTaskCreate(encoder_task, "encoder_task", 256, &mainqueue, 2, NULL);

}
